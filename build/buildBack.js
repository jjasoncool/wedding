// CSS
import "./stylesback.scss";
import "../stastic/fonts/font-awesome-4.7.0/css/font-awesome.min.css";

// JS
import "bootstrap";
import "select2";
import {TabulatorFull as Tabulator} from 'tabulator-tables';

import backstage from "../stastic/js/backstage";

backstage({
  Tabulator,
});
