// CSS
import "./styles.scss";
import "../stastic/css/main.css";
import "../stastic/fonts/font-awesome-4.7.0/css/font-awesome.min.css";

// JS
import "bootstrap";
import jQueryBridget from "jquery-bridget";
import "jquery.easing";
import "circletype";

import Vivus from "vivus";
window.Vivus = Vivus;

import "slick-carousel";

import Masonry from "masonry-layout";
jQueryBridget("masonry", Masonry, $);

import "lightbox2";
import "howler";
import main from "../stastic/js/main.js";

main();
