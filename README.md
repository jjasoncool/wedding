
## 建立 server ##
- `docker-compose up -d`

## 創建資料庫 ##
- yarn migrate:create 名稱
- schema 設定檔會在`./db/migrations/`產出檔案，請依據需求設定資料
- 創好的資料庫會放在 `./db/data.db`，此位置設定在 `knexfile.js` 內 請記得用 chmod 改成 664
- 執行 `yarn knex migrate:latest` 或者 `yarn knex migrate:up` 依據schema建立table
- 執行 `yarn knex migrate:down` 移除table

## 創建資料庫資料 ##
- yarn seed:create 名稱
- seed 設定檔會在`./db/seeds/`產出檔案，請依據需求設定資料
- 執行 `yarn knex seed:run`
