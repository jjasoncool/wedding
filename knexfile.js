// A knexfile.js generally contains all of the configuration for your database

// this line reads .env for knex cmd
require("dotenv").config();

module.exports = {
  development: {
    // some other config
  },
  production: {
    client: "sqlite3",
    connection: {
      filename: "./db/data.db",
    },
    migrations: {
      tableName: "migrations",
      directory: "db/migrations",
    },
    seeds: {
      directory: "db/seeds",
    },
    useNullAsDefault: true,
  },
};
