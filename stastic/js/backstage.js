/**
 * 收禮 script
 */

function backstage({ Tabulator }) {
  $("body").on("keypress", function (event) {
    if (event.key === "Enter") {
      // 13 是 enter 鍵的 keyCode
      $("#submitBtn, #search-btn, #recMoney").trigger("click");
    }
  });

  $("#selectGuestId")
    .select2({
      placeholder: "請輸入姓名",
      allowClear: true,
      theme: "bootstrap4",
      ajax: {
        dataType: "json",
        url: "./search",
        type: "GET",
        delay: 250,
        data: function (params) {
          return {
            name: params.term, // search term
            // page: params.page,
          };
        },
        processResults: function (data, params) {
          params.page = params.page || 1;

          return {
            results: data,
            // .map((datum) => {
            //   if (datum.parent) {
            //     return {
            //       ...datum,
            //       id: datum.parent.id,
            //     };
            //   }

            //   return datum;
            // })
            pagination: {
              more: params.page * 30 < data.total_count,
            },
          };
        },
      },
      templateResult: function formatRepo(data) {
        if (data.loading) {
          return "loading";
        }

        var $container = $(
          "<div class='select2-result-repository clearfix'>" +
            "<div class='select2-result-repository__name'></div>" +
            "</div>"
        );

        $container
          .find(".select2-result-repository__name")
          .text(`${data.name}${data?.parent?.name ? `(${data?.parent?.name})` : ""}`);

        return $container;
      },
      templateSelection: function formatRepoSelection(data) {
        return data?.id
          ? `${data.name}${data?.parent?.name ? `(${data?.parent?.name})` : ""}`
          : "請輸入姓名";
      },
    })
    .on("select2:open", () => {
      const select2Input = document.querySelector(".select2-search__field");
      focusTo(select2Input);
      $(select2Input).prop("placeholder", "點此搜尋");
    });

  $("#showIdName")
    .select2({
      placeholder: "請輸入來賓ID",
      allowClear: true,
      theme: "bootstrap4",
      ajax: {
        dataType: "json",
        url: "./search",
        type: "GET",
        delay: 250,
        data: function (params) {
          return {
            id: params.term, // search term
            // page: params.page,
            parentOnly: 1,
          };
        },
        processResults: function (data, params) {
          params.page = params.page || 1;

          return {
            results: data,
            pagination: {
              more: params.page * 30 < data.total_count,
            },
          };
        },
      },
      templateResult: function formatRepo(data) {
        if (data.loading) {
          return "loading";
        }

        var $container = $(
          "<div class='select2-result-repository clearfix'>" +
            "<div class='select2-result-repository__name'></div>" +
            "</div>"
        );

        $container
          .find(".select2-result-repository__name")
          .text(`${data.id} - ${data.name}${data?.parent?.name ? `(${data?.parent?.name})` : ""}`);

        return $container;
      },
      templateSelection: function formatRepoSelection(data) {
        return data?.id
          ? `${data.id} - ${data.name}${data?.parent?.name ? `(${data?.parent?.name})` : ""}`
          : "請輸入來賓ID";
      },
    })
    .on("select2:open", () => {
      const select2Input = document.querySelector(".select2-search__field");
      focusTo(select2Input);
      $(select2Input).prop("placeholder", "點此搜尋");
    });

  const focusTo = function (element) {
    element.dispatchEvent(new FocusEvent("focus"));
  };

  // 臨時參加
  $("#accountantForm").on("submit", function (e) {
    const addName = $("#temporary-participant").val().trim();
    if (addName !== "" && $("#showIdName").val() === "") {
      if (!window.confirm(`請確認是否要新增臨時參與人員 ${addName}?`)) {
        e.preventDefault();
      }
    }
  });

  $("#submitBtn").on("click", function (event) {
    // event.preventDefault(); // prevent default form submission
    const attendance = parseInt(document.querySelector("#attendance").value);
    const urlParams = new URLSearchParams(window.location.search);
    const id = parseInt(urlParams.get("id"));
    const parentId = parseInt(document.querySelector("#parent_id").value); // may not exists
    const newWindow = window.open();

    $.ajax({
      url: "./checkIn",
      type: "POST",
      data: JSON.stringify({ id: parentId ? parentId : id, attendance, myid: id }),
      contentType: "application/json",
      success: function (data) {
        newWindow.location = "./seats?tableNum=" + data;
      },
      error: function (jqXHR, textStatus, errorThrown) {
        // do something to notify the user to retry???
        console.log("Error: " + errorThrown);
      },
    });
  });

  $("#recMoney").on("click", function (event) {
    // event.preventDefault(); // prevent default form submission
    const gift_money = parseInt(document.querySelector("#gift_money").value); // should be int???
    const id = parseInt(document.querySelector("#gift_id").value); // should be int???
    // const urlParams = new URLSearchParams(window.location.search);
    // const id = parseInt(urlParams.get("id"));

    $.ajax({
      url: "./recMoney",
      type: "POST",
      data: JSON.stringify({ id, gift_money }),
      contentType: "application/json",
      success: function () {
        window.location = "./recStatus";
      },
      error: function (jqXHR, textStatus, errorThrown) {
        // do something to notify the user to retry???
        console.log("Error: " + errorThrown);
      },
    });
  });

  const table$ = document.querySelector("#recDetailTable");

  if (table$) {
    // table
    const resutlTable = new Tabulator("#recDetailTable", {
      layout: "fitColumns",
      selectable: false,
      ajaxURL: "./recStatus/", //ajax URL
      ajaxConfig: "POST", //ajax HTTP request type
      ajaxContentType: "json", // send parameters
      columns: [
        { title: "號碼", field: "id", hozAlign: "left" },
        { title: "姓名", field: "name", hozAlign: "left" },
        { title: "禮金", field: "cash_gift", hozAlign: "right" },
        {
          title: "收禮時間",
          field: "accountant_time",
          hozAlign: "right",
          sorter: function (a, b) {
            if (a === b) {
              return 0;
            }
            if (!a) {
              return -1;
            }
            if (!b) {
              return 1;
            }
            return new Date(a) - new Date(b);
          },
        },
        { title: "是否收餅", field: "receive", hozAlign: "center" },
      ],
      ajaxResponse: function (url, params, response) {
        //url - the URL of the request
        //params - the parameters passed with the request
        //response - the JSON object returned in the body of the response.

        response = response.filter((element) => element.parent_id == null);
        response.total = response.rctotal = 0;
        response.forEach((element) => {
          // 總和
          response.total += element.cash_gift;
          if (element.cash_gift > 0) {
            response.rctotal++;
          }
          // 轉成千分位有逗點的數字
          element.cash_gift = element.cash_gift.toLocaleString();
          element.receive =
            element.receive == 1
              ? "已收餅"
              : parseInt(element.wedding_cake) + parseInt(element.chinese_cake) > 0
              ? "未收餅"
              : "無囍餅";
        });

        return response; //return the response data to tabulator
      },
      footerElement: `<div class="text-right container-fluid">收禮人數: <span id='rctotal'></span></div>`,
    });

    resutlTable.on("dataProcessed", (data) => {
      document.getElementById("total").innerText = data.total.toLocaleString();
      document.getElementById("rctotal").innerText = data.rctotal.toLocaleString();
      resutlTable.setSort([
        {
          column: "accountant_time",
          dir: "desc",
        }, //sort by this first
      ]);
      resutlTable.selectRow(resutlTable.getRowFromPosition(1));
    });
  }

  // 座位表
  const location = window.location.href.match(/wedding\/(.*?)\/seats/)?.[1];

  if (location?.length) {
    const canvas = document.getElementById("seat");
    const canvasContainer = document.getElementById("seat-container");
    const ctx = canvas.getContext("2d");
    const seatImg = new Image();
    // 圖片長寬
    let seatImgWidth = 0;
    let seatImgHeight = 0;
    // 依據ID回傳桌次資料
    let canvasInfos = {};
    // 縮圖比例
    let widthRatio = 1;
    // 目前繪圖位置
    let currentPos = {
      x: 0,
      y: 0,
    };

    seatImg.onload = () => {
      // needs after onload
      seatImgWidth = seatImg.naturalWidth;
      seatImgHeight = seatImg.naturalHeight;

      // 取得外框寬度算比例
      const parentWidth =
        canvasContainer.clientWidth -
        parseInt(getComputedStyle(canvasContainer).paddingLeft) -
        parseInt(getComputedStyle(canvasContainer).paddingRight);
      widthRatio = parentWidth / seatImgWidth;

      canvas.width = seatImgWidth * widthRatio;
      canvas.height = seatImgHeight * widthRatio;

      ctx.scale(widthRatio, widthRatio);
      ctx.drawImage(seatImg, 0, 0);

      if (document.getElementById("seat-info").value.length !== 0) {
        try {
          canvasInfos = JSON.parse(JSON.parse(document.getElementById("seat-info").value).info);
          drawCircle(canvasInfos.x_pos, canvasInfos.y_pos, canvasInfos.radius, {
            color: canvasInfos.color,
          });
          currentPos = {
            x: canvasInfos.x_pos,
            y: canvasInfos.y_pos,
          };
        } catch (error)  {
          console.log(error.msg);
        }
      }
    };

    seatImg.src = `/wedding/img/seat-plan/${location}.png`;

    /**
     * 拖曳功能
    const rect = canvas.getBoundingClientRect();
    let isMouseDown = false;
    canvas.addEventListener("mousedown", function (event) {
      isMouseDown = true;
    });

    canvas.addEventListener("mouseup", function (event) {
      isMouseDown = false;
      console.table(currentPos);
      console.table(rect);
    });

    canvas.addEventListener("mousemove", (event) => {
      if(!isMouseDown) {
        return
      }
      // 重新放圖，不然會變成印章效果
      ctx.drawImage(seatImg, 0, 0);
      // 計算物件位置
      currentPos.x = event.clientX / widthRatio - rect.left + parseInt(getComputedStyle(canvasContainer).paddingLeft);
      currentPos.y = event.clientY / widthRatio - rect.top - parseInt(getComputedStyle(canvasContainer).paddingTop);

      drawCircle(currentPos.x, currentPos.y, canvasInfos.radius, {
        color: canvasInfos.color,
      });
    });
    // */

    function drawCircle(x, y, radius, options = {}) {
      options = Object.assign({ lineWidth: 10, color: "red" }, options);
      ctx.beginPath();
      ctx.arc(x, y, radius, 0, Math.PI * 2);
      ctx.strokeStyle = options.color;
      ctx.lineWidth = options.lineWidth;
      ctx.stroke();
    }
  }
}

export default backstage;
