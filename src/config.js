const channelUrl = "https://www.youtube.com/channel/UCA96h_Icwq8ivbBIrodFfZw";

const WEDDING_TP = {
  id: 1,
  city: "Taipei, Taiwan",
  youtube: channelUrl,
  date: new Date("2023-05-06T12:00:00.000+08:00"),
  place: "台北大直典華飯店3F璀璨廳",
  tel: "02-8502-5555",
  location: "台北市中山區植福路8號",
  time: "12:00",
  mapUrl: "https://goo.gl/maps/yMpF9JhUjSqYCFY98",
  hotelPhoto: "HotelTP.jpg",
  traffic: `<p><b style="font-weight: bold;">自行開車</b><br> 大直典華B2停車場(車位有限請見諒) </p>
            <p><b style="font-weight: bold;">捷運資訊</b><br> 文湖線 劍南路站2號出口 (步行約27秒)</p>
            <p><b style="font-weight: bold;">公車資訊</b><br> 捷運劍南路站(步行3分鐘) 28 33 72 208 222 287 247 256 267 268 286 553 620 646 645 902 556 683 681 957 955 1801 1573 紅2 紅3 棕20 藍7 藍20(區) 藍26 綠16 內湖幹線 </p>
            <p><b style="font-weight: bold;">美麗華接駁公車</b><br> 淡水線劍潭站至美麗華<br>(步行約5分鐘) <small>*以美麗華網站資訊為主</small></p>`,
  stay: "",
  activities: `<div>
                <p style="line-height: 0.9"><i class="fa fa-snowflake-o" aria-hidden="true"></i> <b style="font-weight: bold;">證婚儀式(10:00典華圓心)</b> <br>
                  <span style="font-size: 16px; padding-left: 2rem;">請填寫<u><a class="page-scroll" href="#rsvp">下面表單</a></u>報名參加</span><br>
                  <span style="font-size: 16px; padding-left: 2rem;">座位有限請盡速報名</span>
                </p>
                <p style="line-height: 0.9"><i class="fa fa-snowflake-o" aria-hidden="true"></i> <b style="font-weight: bold;">Candy Bar</b> <br>
                  <span style="font-size: 16px; padding-left: 2rem;">婚宴12點入席</span><br>
                  <span style="font-size: 16px; padding-left: 2rem;">建議準時抵達，先輕鬆地敘舊&享用小點心</span>
                </p>
                <p style="line-height: 0.9"><i class="fa fa-snowflake-o" aria-hidden="true"></i> <b style="font-weight: bold;">互動遊戲(My Big Day)</b> <br>
                  <span style="font-size: 16px; padding-left: 2rem;">請先準備好您的手機</span><br>
                  <span style="font-size: 16px; padding-left: 2rem;">並且記得要充飽電</span>
                </p>
                <p style="line-height: 0.9"><i class="fa fa-snowflake-o" aria-hidden="true"></i> <b style="font-weight: bold;">拍貼機</b> <br>
                  <span style="font-size: 16px; padding-left: 2rem;">請先準備好您的手機多拍幾張美美的照片</span><br>
                  <span style="font-size: 16px; padding-left: 2rem;">不用考慮太多，請大家盡量拍照印出來</span><br>
                  <span style="font-size: 16px; padding-left: 2rem;">給予我們滿滿祝福<i class="fa fa-heart" aria-hidden="true"></i>一起留下可愛的回憶</span>
                </p>
              </div>`,
  wishes: {
    娣娣: {
      format: "png",
      Greetings:
        "潔汶、勝光，希望你們在高雄的生活都能順順利力，未來鴻圖大展，早生貴子！期待婚禮見到美新娘帥新郎",
    },
    友謙: {
      format: "jpg",
      Greetings: "恭喜唷！百年好合",
    },
    泓翔: {
      format: "jpg",
      Greetings: "祝白頭偕老、永浴愛河～～～",
    },
    郡儀: {
      format: "jpg",
      Greetings: "潔汶～新婚愉快～～祝福你們永浴愛河，永遠幸福🥰",
    },
    LowerTu: {
      format: "jpg",
      Greetings: "恭喜勝光～ 預祝你們 百年好合",
    },
    嘉倫: {
      format: "png",
      Greetings: "祝福兩位能一起開創出美好的生活！",
    },
    "斯寬&月月": {
      format: "jpg",
      Greetings:
        "可以看到勝光哥還有潔汶一起步入禮堂真的好開心💕相信你們一定可以感情長長久久白頭偕老❤️期待你們早生貴子生一打的娃娃讓我們玩耍唷🥰要一直幸福下去❤️💜💚💛💙",
    },
    芳瑀teacher: {
      format: "jpg",
      Greetings: "恭喜！！ 一定要幸福喔❤️",
    },
    奉達: {
      format: "jpg",
      Greetings: "祝大隊長甜蜜幸福～！！！",
    },
    永安: {
      format: "jpg",
      Greetings: "勝光潔汶要幸福喔！",
    },
    勁甫: {
      format: "jpg",
      Greetings: "哈哈哈哈～早生貴子  想你啊～勝光大大",
    },
    書偉哥: {
      format: "jpg",
      Greetings: "恭喜詹大勝光！",
    },
    詠筑: {
      format: "jpg",
      Greetings: "祝阿光跟潔汶百年好合 一直幸福&lt;3",
    },
    哆啦: {
      format: "jpg",
      Greetings: "恭喜兩位XD",
    },
    Joanne: {
      format: "jpg",
      Greetings: "百年好合，早生貴子",
    },
    雅婷: {
      format: "jpg",
      Greetings: "早生貴子XDD",
    },
    佳如: {
      format: "jpg",
      Greetings: "很為妳感到高興，要開心過未來的每一天喔～",
    },
    智恩: {
      format: "jpg",
      Greetings: "恭喜你們~祝婚姻美滿!",
    },
    文正大哥: {
      format: "jpg",
      Greetings: "恭喜你們，祝福你們永浴愛河，幸福快樂",
    },
    Shawn: {
      format: "jpg",
      Greetings: "國家需要你，趕緊多生幾個！",
    },
    Peggy: {
      format: "png",
      Greetings: "要一直幸福快樂喔⋯⋯",
    },
    晨語: {
      format: "jpg",
      Greetings:
        "恭喜可愛的新娘跟新郎要一起攜手前往人生的下一個階段~祝福你們之後的日子都要開開心心的唷~",
    },
    亨亨: {
      format: "jpg",
      Greetings: "相當不可思議的一對，籌備婚禮太忙碌了，婚禮完要開啟無限耍廢模式！！",
    },
    小夏: {
      format: "jpg",
      Greetings:
        '"經營婚姻生活秘訣 必定要互相溝通與包容 一起不斷成長祝福你們在婚姻生活創造更多幸福的點滴♥♥♥"',
    },
    黃霈: {
      format: "jpg",
      Greetings: "終於走到這天喔 恭喜欸",
    },
  },
};

const WEDDING_KH = {
  id: 2,
  city: "Kaohsiung, Taiwan",
  youtube: channelUrl,
  date: new Date("2023-04-30T18:00:00.000+08:00"),
  place: "高雄漢來飯店9F金鳳廳",
  tel: "07-2135799",
  location: "高雄市前金區成功一路266號",
  time: "18:00",
  mapUrl: "https://goo.gl/maps/GjvGitL4e5SWnuqP9",
  hotelPhoto: "HotelKH.jpg",
  traffic: `<p><b style="font-weight: bold;">自行開車</b><br> 國道1號 → 下高雄交流道367B出口(中正路出口) → 右轉沿著中正一路直行 → 左轉中山一路 → 再右轉新田路 → 漢來大飯店就在左手邊，並附有停車場</p>
            <p><b style="font-weight: bold;">捷運資訊</b><br> 搭乘【捷運紅線】，從【高鐵左營站R16】搭至【中央公園站R9】1號出口，再步行約7分鐘抵達漢來大飯店。</p>`,
  stay: "",
  activities: `<div>
                <p style="line-height: 0.9"><i class="fa fa-snowflake-o" aria-hidden="true"></i> 互動遊戲(My Big Day) <br>
                  <span style="font-size: 16px; padding-left: 2rem;">請先準備好您的手機</span><br>
                  <span style="font-size: 16px; padding-left: 2rem;">並且記得要充飽電</span>
                </p>
                <p style="line-height: 0.9"><i class="fa fa-snowflake-o" aria-hidden="true"></i> 拍貼機 <br>
                  <span style="font-size: 16px; padding-left: 2rem;">請先準備好您的手機多拍幾張美美的照片</span><br>
                  <span style="font-size: 16px; padding-left: 2rem;">不用考慮太多，請大家盡量拍照印出來</span><br>
                  <span style="font-size: 16px; padding-left: 2rem;">給予我們滿滿祝福<i class="fa fa-heart" aria-hidden="true"></i>一起留下可愛的回憶</span>
                </p>
              </div>`,
  wishes: {
    文妤: {
      format: "jpg",
      Greetings: "能夠遇到願意與你攜手共度人生的人是件幸運的事，祝福潔汶幸福美滿!",
    },
    輝榮乾爹: {
      format: "jpg",
      Greetings: "永結同心，要幸福喔⋯⋯",
    },
    伊貝: {
      format: "jpg",
      Greetings: "白頭偕老，健康快樂的生個可愛的寶寶給我玩<br>(´･ω･`)",
    },
    盛安: {
      format: "jpg",
      Greetings: "恭喜~ 祝福你能每天都比前一天更進步一些!",
    },
    易廷: {
      format: "jpg",
      Greetings: "恭喜老爺恭喜夫人",
    },
    子軒: {
      format: "jpg",
      Greetings: "恭喜你們！百年好合唷啾咪啾咪&lt;3",
    },
    雅惠阿姨: {
      format: "jpg",
      Greetings: "恭賀你們新婚快樂！🎉🎉",
    },
    "芯慈&辰安Family": {
      format: "jpg",
      Greetings: "恭喜大姐姐結婚～祝百年好合、幸福快樂！",
    },
    "亮竹&亮印Family": {
      format: "png",
      Greetings: "恭喜！祝福百年好合！",
    },
    祖安: {
      format: "jpg",
      Greetings: "潔汶＆勝光遇見生命裡的喜悅、插曲、挑戰和平靜，都願意攜手走過❤️",
    },
    子萍: {
      format: "jpg",
      Greetings: "好期待看到你們 幸福的樣子🥰🥰🥰",
    },
    巧涵: {
      format: "jpg",
      Greetings: "呀～～終於等到你們的婚禮！要幸福一輩子唷😍😍😍",
    },
    佩青: {
      format: "jpg",
      Greetings: "潔汶祝福你，跟你的先生長長久久，終於等到你們的婚禮，也祝福你有美好的婚姻❤️",
    },
    鈴鈴: {
      format: "jpg",
      Greetings: "未來的日子都要開開心心🥰",
    },
    "芯旎&冠旖Family": {
      format: "jpg",
      Greetings: "謝謝大姐姐照顧冠冠，我們一家都愛你們❤️💕💜永浴愛河、白頭偕老🥰",
    },
    "俞心&于安Family": {
      format: "jpg",
      Greetings: "恭喜~新婚愉快",
    },
    愛心: {
      format: "jpg",
      Greetings: "HAPPY WEDDING!",
    },
  },
};

const DB = {
  client: "sqlite3",
  connection: {
    filename: "./db/data.db",
    // 設定時區為 UTC+8 (但 sqlite 無效)
    timezone: "+08:00",
  },
  useNullAsDefault: true,
};

module.exports = { WEDDING_TP, WEDDING_KH, DB };
