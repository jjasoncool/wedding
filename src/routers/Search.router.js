// 搜尋資料
const Router = require("@koa/router");
const { ReceptionServiceInstance } = require("../services");

const router = new Router();

// 這邊只回傳 json
router.get("/search", async (ctx) => {
  const { location } = ctx.params;
  const { id, name, parentOnly } = ctx.request.query;

  // 如果文字parseInt = NaN
  const results = await ReceptionServiceInstance.search(
    id && !isNaN(parseInt(id)) ? parseInt(id) : name ? name : id,
    location,
    parentOnly
  );

  ctx.body = JSON.stringify(results);
});

module.exports = router;
