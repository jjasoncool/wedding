const Router = require("@koa/router");
const router = new Router();

router.all('(.*)', async (ctx) => {
  ctx.status = 404;
  ctx.body = '<html><head><title>Not Found</title></head><body><h1>404 Not Found</h1><p>The requested location was not found.</p></body></html>';
});

module.exports = router;
