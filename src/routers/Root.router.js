const Router = require("@koa/router");

const router = new Router();

router.get("/", async function (ctx) {
  // const users = [{ name: 'Dead Horse' }, { name: 'Imed Jaberi' }, { name: 'Tom' }]
  // await ctx.render('index', { users })
  // console.log("qqqq", ctx.request.query)

  const setting = { config: ctx.config, writeResp: false };
  const body = await ctx.render("index", setting);
  const nav = await ctx.render("navs/index_nav", setting);

  await ctx.render("layouts/template", {
    config: ctx.config,
    body,
    nav,
  });
});

module.exports = router;
