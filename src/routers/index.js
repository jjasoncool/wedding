const Router = require("@koa/router");

const importRouter = require("./Import.router.js");
const receptionRouter = require("./Reception.router.js");
const accRouter = require("./Accountant.router.js");
const cakeRouter = require("./Cakes.router.js");
const searchRouter = require("./Search.router.js");
const pageRouter = require("./Page.router.js");
const rootRouter = require("./Root.router.js");
const notFoundRouter = require("./notFound.router.js");

const locationBasedAPI = new Router({
  prefix: "/:location",
});

const normalAPI = new Router();

const locationBasedRouters = {
  receptionRouter,
  accRouter,
  cakeRouter,
  searchRouter,
};

const otherRouters = {
  importRouter,
  rootRouter,
  pageRouter,
  // final route
  notFoundRouter,
};

const checkLocation = async (ctx, next) => {
  const location = ctx.params.location;
  if (location !== "kh" && location !== "tp") {
    ctx.redirect('/wedding/404');
    return;
  }
  await next();
};

Object.values(locationBasedRouters).forEach((router) =>
  locationBasedAPI.use(checkLocation).use(router.routes())
);
Object.values(otherRouters).forEach((router) => normalAPI.use(router.routes()));

module.exports = {
  locationBasedAPI,
  normalAPI,
};
