const Router = require("@koa/router");
const { ReceptionServiceInstance } = require("../services");
const { stringToNumber, stringOrNull } = require("../helpers/stringParser");
const parseBoolean = require("../helpers/parseBoolean");

const multer = require("@koa/multer");
const { parse } = require("csv-parse");

const router = new Router();
const upload = multer();

router.get("/import", async (ctx) => {
  await ctx.render("import");
});

router.post("/import", upload.single("nameList"), async (ctx) => {
  // const rqbody = ctx.request.body;
  // const query = ctx.request.query;
  const parser = parse({
    delimiter: ",",
  });
  const records = [];

  // Use the readable stream api to consume records
  parser.on("readable", function () {
    let record;
    while ((record = parser.read()) !== null) {
      records.push(record);
    }
  });
  // Catch any error
  parser.on("error", function (err) {
    console.error(err.message);
  });
  // Test that the parsed records matched the expected records
  parser.on("end", async function () {
    // Data transfrom
    const rows = records
      .slice(1) // drop first heading row `[ 'ID', 'name', 'parent_id' ]`
      .map(
        ([
          idString,
          name,
          parentIdString,
          check_in,
          cash_gift,
          wedding_cake,
          chinese_cake,
          cake_box_color,
          region,
          receive,
          table_seating,
          modify_time,
        ]) => {
          return {
            id: stringToNumber(idString),
            name,
            parent_id: stringToNumber(parentIdString),
            check_in: stringToNumber(check_in),
            cash_gift: stringToNumber(cash_gift) ?? 0,
            wedding_cake: stringToNumber(wedding_cake) ?? 0,
            chinese_cake: stringToNumber(chinese_cake) ?? 0,
            cake_box_color,
            region,
            receive: parseBoolean(receive),
            table_seating,
            modify_time: stringOrNull(modify_time),
          };
        }
      );

    // Insert to db
    await ReceptionServiceInstance.insertMany(rows);
  });

  parser.write(ctx.request.file.buffer.toString("utf8"));

  parser.end();

  ctx.body = `
    <h1>DONE</h1>
  `;
});

// export router，暫時先放這，匯出資料
router.get("/export", async (ctx) => {
  // if table is larrrrrrrrrrrrrge, use stream, don't get all here
  const records = await ReceptionServiceInstance.getAll();

  ctx.set("Content-Disposition", 'attachment; filename="data.csv"');
  ctx.set("Content-Type", "text/csv");
  ctx.body = [
    {
      id: "id",
      name: "name",
      parent_id: "parent_id",
      check_in: "check_in",
      cash_gift: "cash_gift",
      wedding_cake: "wedding_cake",
      chinese_cake: "chinese_cake",
      cake_box_color: "cake_box_color",
      region: "region",
      receive: "receive",
      table_seating: "table_seating",
      modify_time: "modify_time",
      accountant_time: "accountant_time",
    },
    ...records,
  ]
    .map((record) =>
      [
        record.id,
        record.name,
        record.parent_id,
        record.check_in,
        record.cash_gift,
        record.wedding_cake,
        record.chinese_cake,
        record.cake_box_color,
        record.region,
        record.receive,
        record.table_seating,
        record.modify_time,
        record.accountant_time,
      ].join(",")
    )
    .join("\n");
});

module.exports = router;
