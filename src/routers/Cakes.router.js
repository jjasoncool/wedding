const Router = require("@koa/router");
const { ReceptionServiceInstance } = require("../services");

const router = new Router();

router.get("/cakes", async (ctx) => {
  const query = ctx.request.query;
  const setting = { config: ctx.config, writeResp: false };
  let body = await ctx.render("cakes", setting);

  if (query?.id) {
    const person = await ReceptionServiceInstance.getRecordById(query?.id);
    // 頁面顯示邏輯

    // 渲染
    body = await ctx.render("cake_received", {
      ...person,
      ...setting,
    });
  }

  await ctx.render("layouts/rec_template", {
    body,
  });
});

// 提示領取囍餅頁
router.get("/receiveCakes", async (ctx) => {
  const query = ctx.request.query;
  const setting = { config: ctx.config, writeResp: false };
  let person = {};

  if (query?.id) {
    person = await ReceptionServiceInstance.getRecordById(query?.id);
    try {
      await ReceptionServiceInstance.updateRecordById(query?.id, {
        receive: 1,
      }, "receiveCakes");

      ctx.status = 200;
    } catch (error) {
      console.log(error);

      ctx.status = 500;
    }
  } else {
    // 404
    ctx.redirect("/wedding/404");
    return;
  }

  let body = await ctx.render("cake_received", {
    ...person,
    ...setting,
    received: true,
  });

  await ctx.render("layouts/rec_template", {
    body,
  });
});

module.exports = router;
