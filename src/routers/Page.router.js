const Router = require("@koa/router");

const router = new Router();

// for create credit card
// router.get("/blog", async (ctx) => {
//   const setting = {config: ctx.config, writeResp: false}
//   const body = await ctx.render("blog", setting)
//   const nav = await ctx.render("navs/sub_nav", setting)

//   await ctx.render('layouts/template', {
//     body,
//     nav,
//   });
// });

const navMap = {
  blog: "navs/sub_nav",
  "blog-single": "navs/sub_nav",
  gallery: "navs/sub_nav",
  rsvp: "navs/sub_nav",
};

// test
router.get("/:page", async (ctx, next) => {
  const page = ctx.params.page;
  const navPath = navMap[page];

  if (!navPath) {
    ctx.status = 404;
    // 下個 middleware (下一個 route)
    return next();
  }

  const setting = { config: ctx.config, writeResp: false };
  const body = await ctx.render(page, setting);
  const nav = await ctx.render(navPath, setting);

  await ctx.render("layouts/template", {
    config: ctx.config,
    body,
    nav,
  });
});

module.exports = router;
