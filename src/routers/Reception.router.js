const Router = require("@koa/router");
const { ReceptionServiceInstance, CanvasMarkServiceInstance } = require("../services");

const router = new Router();

router.get("/reception", async (ctx) => {
  const query = ctx.request.query;
  const setting = { config: ctx.config, writeResp: false };
  let body = await ctx.render("reception", setting);

  if (query?.id) {
    const person = await ReceptionServiceInstance.getRecordById(query?.id);
    let parent = {};
    // search parent
    if (person?.parent_id) {
      parent = await ReceptionServiceInstance.getRecordById(person.parent_id);
    } else {
      parent = person;
    }

    body = await ctx.render("reception_with_id", {
      person,
      parent,
      ...setting,
    });
  }

  await ctx.render("layouts/rec_template", {
    body,
  });
});

router.post("/checkIn", async (ctx) => {
  const { body } = ctx.request;

  try {
    // 這邊接到parentid
    await ReceptionServiceInstance.updateRecordById(body.id, {
      check_in: body?.attendance ?? 0,
    }, "checkIn");
    const person = await ReceptionServiceInstance.getRecordById(body.myid);

    ctx.status = 200;
    ctx.body = person.table_seating;

    return;
  } catch (error) {
    console.log(error);

    ctx.status = 500;
  }

  ctx.body = "";
});

router.get("/seats", async (ctx) => {
  const { location } = ctx.params;
  const { tableNum } = ctx.request.query;
  const drawInfo = await CanvasMarkServiceInstance.getDrawByTableNum(tableNum, location);

  await ctx.render("seating-plan", { location, drawInfo });
});

module.exports = router;
