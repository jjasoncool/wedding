const Router = require("@koa/router");
const { ReceptionServiceInstance } = require("../services");
const { format } = require("date-fns");

const router = new Router();

router.get("/accountant", async (ctx) => {
  const query = ctx.request.query;
  const setting = { config: ctx.config, writeResp: false };
  let body = await ctx.render("accountant", setting);

  if (query?.id) {
    const person = await ReceptionServiceInstance.getRecordById(query?.id);
    console.log(person);
    body = await ctx.render("accountant_receive", {
      ...person,
      ...setting,
    });
  } else if (query?.addName) {
    const { location } = ctx.params;
    const personRow = await ReceptionServiceInstance.insert({
      name: query?.addName,
      region: location,
    });
    console.log(personRow);
    const person = await ReceptionServiceInstance.getRecordById(...personRow);
    body = await ctx.render("accountant_receive", {
      ...person,
      ...setting,
    });
  }

  await ctx.render("layouts/rec_template", {
    body,
  });
});

router.post("/recMoney", async (ctx) => {
  const { body } = ctx.request;

  try {
    await ReceptionServiceInstance.updateRecordById(body.id, {
      cash_gift: body?.gift_money ?? 0,
      accountant_time: format(new Date(), "yyyy-MM-dd HH:mm:ss"),
    }, "receiveMoney");

    ctx.status = 200;
  } catch (error) {
    console.log(error);

    ctx.status = 500;
  }

  ctx.body = "";
});

router.get("/recStatus", async (ctx) => {
  const setting = { config: ctx.config, writeResp: false };

  // direct find table
  let body = await ctx.render("accountant_status", {
    ...setting,
  });
  await ctx.render("layouts/rec_template", {
    body,
  });
});

router.post("/recStatus", async (ctx) => {
  const { location } = ctx.params;

  // 注意: check_in=-1 沒有來的，並預先給禮金
  // 統計需要過濾掉 check_in 預收禮金，避免現場收禮金額錯誤
  const data = await ReceptionServiceInstance.getAll({
    region: ["=", location],
    check_in: ["!=", -1],
  });

  ctx.header["content-type"] = "application/json";
  ctx.body = JSON.stringify(data);
});

module.exports = router;
