require("dotenv").config(); // this line reads .env for app
const Koa = require("koa");
const logger = require("koa-logger");
const render = require("@koa/ejs");
const koaStatic = require("koa-static");
const path = require("path");
const { format } = require("date-fns");
const { koaBody } = require("koa-body");

// routers
const { locationBasedAPI, normalAPI } = require("./routers/index.js");
const { WEDDING_TP, WEDDING_KH } = require("./config.js");

const port = 3000;

const app = new Koa();
app.use(koaBody());
app.use(logger());
render(app, {
  root: path.join(__dirname, "views"),
  layout: null,
  viewExt: "html",
  cache: false,
  debug: false, // This will log all html out
});

const config = {
  taipei: WEDDING_TP,
  kaohsiung: WEDDING_KH,
};

// stastic
app.use(koaStatic(path.join(__dirname, "../stastic")));

// inject ctx.config before route
app.use(async (ctx, next) => {
  const place = ctx.request.query.place ?? "taipei";

  ctx.config = config[place];
  ctx.config.format = format;
  // console.log(ctx.config);

  await next();
});

// router
app.use(locationBasedAPI.routes());
// root is here
app.use(normalAPI.routes());

app.listen(port);
console.log("open port " + port);

app.on("error", function (err) {
  console.log(err.stack);
});
