class StorageService {
  constructor(knex) {
    this.knex = knex;
  }

  async addData(someVal) {
    return await this.knex
      .insert({
        demo: someVal,
      })
      .into("storage");
  }

  // demo only
  async getLastRecord() {
    return await this.knex.select("*").from("storage").orderBy("id", "desc").offset(0).limit(1);
  }

  async getRecordById(id) {
    // destruct the first row
    const [row] = await this.knex
      .select("*")
      .from("storage")
      .where({
        id,
      })
      .offset(0)
      .limit(1);

    if (!row) {
      throw new Error("data not found");
    }

    return row;
  }
}

module.exports = StorageService;
