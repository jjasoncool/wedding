class CanvasMarkService {
  constructor(knex) {
    this.knex = knex;
  }

  // 取得圖案by桌號
  async getDrawByTableNum(Num, region) {
    let json_row = "";
    try {
      const [row] = await this.knex
        .select("*")
        .from("canvas_mark")
        .where({ table_seating: Num, region });
      json_row = JSON.stringify(row);
    } catch (error) {
      console.log(error);
    }

    // row to json

    return json_row;
  }

  // 取得圖案by其他資訊
  async getDrawByInfo(color) {
    const row = await this.knex
      .select("*")
      .from("canvas_mark")
      .whereRaw("json_extract(info, '$.color') = ?", [color]);

    if (!row) {
      throw new Error("data not found");
    }

    // row to json
    const json_row = JSON.stringify(row);

    return json_row;
  }
}

module.exports = CanvasMarkService;
