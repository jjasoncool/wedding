const { format } = require("date-fns");

class ReceptionService {
  constructor(knex) {
    this.knex = knex;
  }

  // TODO: tp reception search kh id need block
  async getRecordById(id) {
    // destruct the first row
    const [row] = await this.knex
      .select("*")
      .from("receptions")
      .where({
        id,
      })
      .offset(0)
      .limit(1);

    if (!row) {
      throw new Error("data not found");
    }

    // 預先估算一家總人數
    const childrenCount = await this.knex("receptions")
      .count("id", { as: "count" })
      .where({ parent_id: id })
      .groupBy("parent_id")
      .first();

    row.check_in_default ??= (childrenCount?.count ?? 0) + 1;

    return row;
  }

  async updateRecordById(id, data, logTyep = "") {
    const updates = { ...data, modify_time: format(new Date(), "yyyy-MM-dd HH:mm:ss") };

    this.knex.transaction(async (trx) => {
      await trx("receptions").where({ id }).update(updates);

      await trx("receptions_log").insert({
        reception_id: id,
        updates,
        log_type: logTyep,
      });
    });
  }

  /*
  data example:
  {
    id: 1,
    name: "lalalal",
    parent_id: null,
  }
  */
  async insert(data) {
    return this.knex.insert(data).into("receptions");
  }

  /*
  rows example:
  [{
    id: 1,
    name: "lalalal",
    parent_id: null,
  }]
  */
  async insertMany(rows) {
    // 不更新禮金、收禮時間、更新時間(但是人員ID一換就要全部重新來)
    return this.knex.raw(
      ` ? ON CONFLICT (id)
      DO UPDATE SET
      name = EXCLUDED.name,
      parent_id = EXCLUDED.parent_id,
      wedding_cake = EXCLUDED.wedding_cake,
      chinese_cake = EXCLUDED.chinese_cake,
      region = EXCLUDED.region,
      table_seating = EXCLUDED.table_seating
      ; `,
      [this.knex("receptions").insert(rows)]
    );

    // return this.knex.batchInsert("receptions", rows);
  }

  /*
  return example;
  [
    {
      id: 1,
      name: "lalalal",
      parent: null
    },
    {
      id: 2,
      name: "lalalal",
      parent: {
        id: 1,
        name: "lalalal",
        parent: null
      }
    },
  ]
  */
  async search(query = "", location, parentOnly = false) {
    let targets = [];
    let sql = this.knex.select("id").select("name").select("parent_id").from("receptions");

    if (typeof query === "string") {
      sql = sql
        .where({
          region: location,
        })
        .whereLike("name", `%${query}%`);
    } else if (typeof query === "number") {
      sql = sql.where({
        id: query,
        region: location,
      });
    } else {
      return [];
    }

    if (parentOnly) {
      sql.whereNull("parent_id");
    }

    targets = await sql;

    const parents_ids = targets
      .map(({ parent_id }) => parent_id)
      .filter((parent_id) => parent_id !== null);

    const parents = await this.knex
      .select("id")
      .select("name")
      .select("parent_id")
      .from("receptions")
      .whereIn("id", parents_ids);

    /*
      indexedParents = [{
        [parent_id as key]: parent info
      }
      ...]
      */
    const indexedParents = {
      // default case
      null: null,
    };

    for (const parent of parents) {
      indexedParents[parent.id] = parent;
    }

    return targets.map((target) => ({
      id: target.id,
      name: target.name,
      parent: indexedParents[target.parent_id],
    }));
  }

  /**
   * return all records
   * @returns
   */
  async getAll(filters = null) {
    const records = this.knex.select("*").from("receptions").whereRaw("1=1");

    if (filters?.region) {
      records.andWhere(function () {
        this.where("region", ...filters.region);
      });
    }

    if (filters?.check_in !== undefined && filters?.check_in !== null) {
      records
        .andWhere(function () {
          this.where("check_in", ...filters.check_in).orWhereNull("check_in");
        })
        .debug();
    }

    return records;
  }
}

module.exports = ReceptionService;
