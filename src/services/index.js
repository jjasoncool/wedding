const { DB } = require("../config");
const CanvasMarkService = require("./CanvasMarkService");
const ReceptionService = require("./ReceptionService");

const knex = require("knex")(DB);

const ReceptionServiceInstance = new ReceptionService(knex); // change it later, demo only
const CanvasMarkServiceInstance = new CanvasMarkService(knex); // change it later, demo only

module.exports = {
  ReceptionServiceInstance,
  CanvasMarkServiceInstance,
};
