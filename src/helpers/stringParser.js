// By ChatGPT, don't ask me

const stringToNumber = (str) => {
  if (str === "null" || str === "") {
    return null;
  } else {
    return Number(str);
  }
};

const stringOrNull = (str) => {
  if (str === "null" || str === "") {
    return null;
  } else {
    return str;
  }
};

module.exports = {
  stringToNumber,
  stringOrNull,
};
