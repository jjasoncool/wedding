/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = async function (knex) {
  await knex.schema.createTable("receptions", function (table) {
    table.increments("id");
    table.string("name", 255).notNullable().comment("姓名");
    table.integer("parent_id").nullable().comment("報到代表人ID，null為代表人本人");
    table.integer("check_in").nullable().defaultTo(0).comment("簽到: {0 : 代表沒來(可能有人會代給禮金), -1: 代表預先已給禮金，不參加}");
    table.integer("cash_gift").defaultTo(0).comment("紅包金額");
    table.integer("wedding_cake").defaultTo(0).comment("西式喜餅");
    table.integer("chinese_cake").defaultTo(0).comment("中式禮餅");
    table.string("cake_box_color", 64).comment("喜餅盒顏色");
    table.string("region", 10).defaultTo("").comment("地區");
    table.boolean("receive").defaultTo(0).comment("是否已收餅");
    table.string("table_seating").defaultTo("").comment("桌次");
  });
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = async function (knex) {
  await knex.schema.dropTable("receptions");

};
