/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = async function (knex) {
  await knex.schema.createTable("canvas_mark", function (table) {
    table.increments("id");
    table.string("table_seating").nullable().defaultTo(0).comment("桌次");
    table.integer("region").nullable().defaultTo(0).comment("地區");
    table.specificType('info', 'json').comment("標記資訊");
  });
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = async function (knex) {
  await knex.schema.dropTable("canvas_mark");

};
