/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function (knex) {
  return knex.schema.hasColumn("receptions_log", "log_type").then(function (exists) {
    if (!exists) {
      return knex.schema.alterTable("receptions_log", function (table) {
        table.string("log_type").defaultTo("").comment("紀錄類型");
      });
    }
  });
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function (knex) {
  return knex.schema.alterTable("receptions_log", function (table) {
    table.dropColumn("log_type");
  });
};
