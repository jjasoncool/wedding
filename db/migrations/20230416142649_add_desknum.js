/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function(knex) {
  return knex.schema.hasColumn("receptions", "table_seating").then(function (exists) {
    if (!exists) {
      return knex.schema.alterTable("receptions", function (table) {
        table.string("table_seating").defaultTo("");
      });
    }
  });
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function(knex) {
  return knex.schema.alterTable("receptions", function (table) {
    table.dropColumn("table_seating");
  });
};
