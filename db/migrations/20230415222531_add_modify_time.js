/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function (knex) {
  return knex.schema.hasColumn("receptions", "modify_time").then(function (exists) {
    if (!exists) {
      return knex.schema.alterTable("receptions", function (table) {
        table.timestamp("modify_time");
      });
    }
  });
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function (knex) {
  return knex.schema.alterTable("receptions", function (table) {
    table.dropColumn("modify_time");
  });
};
