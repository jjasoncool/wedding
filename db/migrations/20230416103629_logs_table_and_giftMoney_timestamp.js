/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = async function (knex) {
  await Promise.all([
    knex.schema.hasColumn("receptions", "accountant_time").then(function (exists) {
      if (!exists) {
        return knex.schema.alterTable("receptions", function (table) {
          table.timestamp("accountant_time");
        });
      }
    }),
    knex.schema.createTable("receptions_log", function (table) {
      table.increments("id");
      table.integer("reception_id").comment("reception table record id");
      table.specificType('updates', 'json');
      table.timestamp("create_time").defaultTo(knex.fn.now());
    }),
  ]);
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = async function (knex) {
  await Promise.all([
    knex.schema.alterTable("receptions", function (table) {
      table.dropColumn("accountant_time");
    }),

    knex.schema.dropTable("receptions_log"),
  ]);
};
