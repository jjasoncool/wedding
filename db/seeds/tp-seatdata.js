/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.seed = async function (knex) {
  await knex("canvas_mark").insert([
    { table_seating: 0, region: "tp", info: JSON.stringify({x_pos: 799, y_pos: 543, radius: 200, color: "red"}) },
    { table_seating: 'A', region: "tp", info: JSON.stringify({x_pos: 197, y_pos: 828, radius: 130, color: "red"}) },
    { table_seating: 'B', region: "tp", info: JSON.stringify({x_pos: 207, y_pos: 1081, radius: 130, color: "red"}) },
    { table_seating: 'C', region: "tp", info: JSON.stringify({x_pos: 201, y_pos: 1424, radius: 130, color: "red"}) },
    { table_seating: 'D', region: "tp", info: JSON.stringify({x_pos: 207, y_pos: 1680, radius: 130, color: "red"}) },
    { table_seating: 'E', region: "tp", info: JSON.stringify({x_pos: 454, y_pos: 679, radius: 130, color: "red"}) },
    { table_seating: 'F', region: "tp", info: JSON.stringify({x_pos: 458, y_pos: 922, radius: 130, color: "red"}) },
    { table_seating: 'G', region: "tp", info: JSON.stringify({x_pos: 449, y_pos: 1162, radius: 130, color: "red"}) },
    { table_seating: 'H', region: "tp", info: JSON.stringify({x_pos: 451, y_pos: 1415, radius: 130, color: "red"}) },
    { table_seating: 'I', region: "tp", info: JSON.stringify({x_pos: 461, y_pos: 1665, radius: 130, color: "red"}) },
    { table_seating: 'J', region: "tp", info: JSON.stringify({x_pos: 1163, y_pos: 777, radius: 130, color: "red"}) },
    { table_seating: 'K', region: "tp", info: JSON.stringify({x_pos: 1170, y_pos: 1030, radius: 130, color: "red"}) },
    { table_seating: 'L', region: "tp", info: JSON.stringify({x_pos: 1176, y_pos: 1283, radius: 130, color: "red"}) },
    { table_seating: 'M', region: "tp", info: JSON.stringify({x_pos: 1179, y_pos: 1525, radius: 130, color: "red"}) },
    { table_seating: 'N', region: "tp", info: JSON.stringify({x_pos: 1399, y_pos: 670, radius: 130, color: "red"}) },
    { table_seating: 'O', region: "tp", info: JSON.stringify({x_pos: 1393, y_pos: 922, radius: 130, color: "red"}) },
    { table_seating: 'P', region: "tp", info: JSON.stringify({x_pos: 1396, y_pos: 1161, radius: 130, color: "red"}) },
    { table_seating: 'Q', region: "tp", info: JSON.stringify({x_pos: 1400, y_pos: 1410, radius: 130, color: "red"}) },
    { table_seating: 'R', region: "tp", info: JSON.stringify({x_pos: 1408, y_pos: 1670, radius: 130, color: "red"}) },
  ]);
};
