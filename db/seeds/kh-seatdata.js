/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.seed = async function (knex) {
  await knex("canvas_mark").insert([
    { table_seating: 0, region: "kh", info: JSON.stringify({x_pos: 819, y_pos: 534, radius: 250, color: "red"}) },
    { table_seating: 1, region: "kh", info: JSON.stringify({x_pos: 256, y_pos: 589, radius: 160, color: "red"}) },
    { table_seating: 2, region: "kh", info: JSON.stringify({x_pos: 258, y_pos: 889, radius: 160, color: "red"}) },
    { table_seating: 3, region: "kh", info: JSON.stringify({x_pos: 253, y_pos: 1180, radius: 160, color: "red"}) },
    { table_seating: 5, region: "kh", info: JSON.stringify({x_pos: 249, y_pos: 1483, radius: 160, color: "red"}) },
    { table_seating: 6, region: "kh", info: JSON.stringify({x_pos: 253, y_pos: 1783, radius: 160, color: "red"}) },
    { table_seating: 7, region: "kh", info: JSON.stringify({x_pos: 258, y_pos: 2064, radius: 160, color: "red"}) },
    { table_seating: 8, region: "kh", info: JSON.stringify({x_pos: 554, y_pos: 846, radius: 160, color: "red"}) },
    { table_seating: 9, region: "kh", info: JSON.stringify({x_pos: 554, y_pos: 1136, radius: 160, color: "red"}) },
    { table_seating: 10, region: "kh", info: JSON.stringify({x_pos: 556, y_pos: 1435, radius: 160, color: "red"}) },
    { table_seating: 11, region: "kh", info: JSON.stringify({x_pos: 551, y_pos: 1729, radius: 160, color: "red"}) },
    { table_seating: 12, region: "kh", info: JSON.stringify({x_pos: 554, y_pos: 2031, radius: 160, color: "red"}) },
    { table_seating: 13, region: "kh", info: JSON.stringify({x_pos: 1066, y_pos: 941, radius: 160, color: "red"}) },
    { table_seating: 15, region: "kh", info: JSON.stringify({x_pos: 1066, y_pos: 1233, radius: 160, color: "red"}) },
    { table_seating: 16, region: "kh", info: JSON.stringify({x_pos: 1073, y_pos: 1539, radius: 160, color: "red"}) },
    { table_seating: 17, region: "kh", info: JSON.stringify({x_pos: 1069, y_pos: 1854, radius: 160, color: "red"}) },
    { table_seating: 18, region: "kh", info: JSON.stringify({x_pos: 1348, y_pos: 801, radius: 160, color: "red"}) },
    { table_seating: 19, region: "kh", info: JSON.stringify({x_pos: 1357, y_pos: 1121, radius: 160, color: "red"}) },
    { table_seating: 20, region: "kh", info: JSON.stringify({x_pos: 1353, y_pos: 1414, radius: 160, color: "red"}) },
    { table_seating: 21, region: "kh", info: JSON.stringify({x_pos: 1354, y_pos: 1715, radius: 160, color: "red"}) },
    { table_seating: 22, region: "kh", info: JSON.stringify({x_pos: 1354, y_pos: 2019, radius: 160, color: "red"}) },
  ]);
};
