require("dotenv").config();
const { DB } = require("../src/config");
const knex = require("knex")(DB);

(async () => {
  let rows = await knex.select("*").from("receptions_log");

  for (const row of rows) {
    const updates = JSON.parse(row.updates);
    if (!updates.accountant_time) {
      continue;
    }

    await knex("receptions").where({ id: row.reception_id }).update({
      accountant_time: updates.accountant_time,
    });

    console.log(updates, row.reception_id);
    // do somethin here
  }
})()
  .then(() => process.exit(0))
  .catch((e) => {
    console.error(e);
    process.exit(1);
  });
